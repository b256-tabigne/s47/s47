const firstNameResult = document.getElementById("firstName")
const lastNameResult = document.getElementById("lastName")
const firstName = document.getElementById("txt-first-name")
const lastName = document.getElementById("txt-last-name")


firstName.addEventListener("keyup", (e) => {
	
	firstNameResult.innerHTML = e.target.value
})

lastName.addEventListener("keyup", (e) => {
	
	lastNameResult.innerHTML = e.target.value
})


